/*console.log("Hello World");
console.log("Jet");
console.log("IDK");

let name = "Jet Gonzalgo";
console.log(name);

let num = 5;
let num2 = 10;

console.log(num);
console.log(num2);

console.log(name, num, num2);

let myVariable;
console.log(myVariable);

myVariable = "new value";
console.log(myVariable);

let bestFinalFantacy = "Final Fantacy x";
console.log(bestFinalFantacy);*/

//to update
/*bestFinalFantacy = "Final Fantacy 7";
console.log(bestFinalFantacy);


const pi = 3.1416;
console.log(pi);*/

/*const plateNum;
console.log(plateNum);*/

/*let name2 = "Edward Cullen";
let role = "Supervisor";

role = "Director";
const tin = "12333-1234";
console.log(name2, role, tin);

let country = "Philippines";
let province = "Metro Manila";
console.log(province, country);

//concatenation - the process to add string by (+)
let address = province + ", " + country;
console.log(address);

let city1 = "Manila";
let city2 = "Copenhagen";
let city3 = "Washinton D.C.";
let city4 = "Tokyo";
let city5 = "New York";
let country1 = "Philippines";
let country2 = "U.S.A.";
let country3 = "South Korea";
let country4 = "Japan";

let capital1 = city1 + ", " + country1;
let capital2 = city3 + ", " + country2;
let capital3 = city4 + ", " + country4;

console.log(capital1);
console.log(capital2);
console.log(capital3);

let number1 = 10;
let number2 = 6;

console.log(number1);
console.log(number2);

let sum1 = number1 + number2;
console.log(sum1);

let sum2 = 16 + 4;
console.log(sum2);

let numString1 = "30";
let numString2 = "50";

let sumString1 = numString1 + numString2;
console.log(sumString1);*/

//interger + string = concatenation
/*let sum3 = number1 + numString1;
console.log(sum3);*///concatenate

//boolean
/*let isAdmin = true;
let isMarried = false;
let isMVP = true;

console.log("Is she married? " + isMarried);
console.log("Is Curry an MVP? " + isMVP);
console.log("Is he the current admin? " + isAdmin);

//arrays [] values are separated by comma (,)
let array1 = ["Goku", "Gohan", "Goten", "Vegeta", "Trunks", "Broly"];
let array2 = ["One Punch Man", "Saitama", true, 5000];
console.log(array1);
console.log(array2);*/

//objects - data type to mimic real world items
//each field in the object is called a property
//each property is separated by comma (,)
//each property is a pair of key: value

/*let hero1 = {

	heroName: "One Punch Man",
	realName: "Saitama",
	incom: 5000,
	isActive: true
};*/

/*let blackPinkMembers = ["Jisso", "Jennie", "Rose", "Lisa"];
let member4 = {

	firstName: "Lalisa",
	lastName: "Manoban",
	isBeautiful: true,
	age: 24
};*/
/*
console.log(blackPinkMembers);
console.log(member4);*/

//null and undefined
//null is the explicit absence of data/value
	//example you search something and found nothing
/*let foundResult = null;
//undefined - there is no value yet
let sampleVariable;//declaration with no initial value

let person2 = {
	name: "Peter",
	age: 42
}*/

//to access the value of the object's property we use dot(.)
//objectName.propertyName

/*console.log(person2.name);
console.log(person2.age);

console.log(person2.isAdmin);//undefined because person2 exist but isAdmin does not
*/
//functions
//function - tell the devise to perform a certain task when called
//fuctions are reusable

/*console.log("Good Afternoon, Everyone! Welcome to my Application");

function greet() {
	console.log("Good Afternoon, Everyone! Welcome to my Application");

}
*/
//function invocation - is when we call our fuction
/*greet();
greet();*/

let favoriteFood = "Fruits";
console.log(favoriteFood);
let sum = 150 + 9;
console.log(sum);
let product = 100 * 90;
console.log(product);
let userIsActive = true;
console.log(userIsActive);

let favoriteRestaurant = ["Jollibee", "Jollibee", "Jollibee", "Jollibee", "Jollibee"];
console.log(favoriteRestaurant);

let favoriteArtist = {
	firstName: "Ji Eun",
	lastName: "Lee",
	stageName: "IU",
	birthDay: "May 16, 1993",
	age: 28,
	bestAlbum: "Lilac",
	bestSong: "Through the Night",
	bestTvShow: "My Mister",
	bestMovie: "Dream",
	isActive: true 
}
console.log(favoriteArtist);

//parameters and arguments
//"name" is called a parameter
//parameter acts as a named variable/container that exists ONLY in the function
//this is used to store information to act as a stand-in or the container of the value passed into the function as an argument

function printName(name) {
	console.log(`My Name is ${name}`)
};

//data passed in the function: argument
//representation of the argument within the function: parameter
printName("Jet");

function displayNum(number) {
	console.log(number);
};

displayNum(3000);
displayNum(3001);

//mini-activity
function displayMessage(msg) {
	console.log(msg);
};

displayMessage("Javascript is Fun?");

//multiple parameter and arguments
	//function can not only recieve a single argument but it can also receive multiple arguments as long as it matches the number of parameters in the function
function displayFullName(firstName, lastName, age) {
	console.log(`${firstName} ${lastName} is ${age}`);
};

displayFullName("Jet", "Gonzalgo", 24);

//return keyword
//return keyword is used so that a function may return a value
//it also stops the process of the function after the return keyword
function createFullName(firstName, middleName, lastName) {
	return `${firstName} ${middleName} ${lastName}`;
	console.log("I will no longer run because the function's value/result has been returned");
};

let fullName1 = createFullName("Tom", "Cruise", "Mapother");
console.log(fullName1);