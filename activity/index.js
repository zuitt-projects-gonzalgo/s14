let fullName = "Steve Rogers";
console.log(fullName);

let age = 40;
console.log(age);
	
let friends = ["Tony", "Bruce", "Thor", "Natasha", "Clint", "Nick"];
console.log(friends);

let profile = {

	userName: "captain_america",
	fullName: "Steve Rogers",
	age: 40,
	isActive: false

};
console.log(profile);

fullName = "Tony Stark";
console.log(fullName);

const largestOcean = "Pacific Ocean";
console.log(largestOcean);

function getSum(num1, num2) {
	return num1 + num2;
};

console.log(getSum(10, 15));